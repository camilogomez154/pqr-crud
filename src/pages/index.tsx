import { AuthorizationPageManager } from './authorization'
import { BrowserRouter } from 'react-router-dom'
import * as React from 'react'

export const RouterManager = () => (
    <BrowserRouter>
        <AuthorizationPageManager />
    </BrowserRouter>
)