import { PrincipalAuthorizationPage } from "./principal"
import { Route, Switch } from "react-router-dom";
import * as React from "react";

export const AuthorizationPageManager = () => 
<Switch>
    <Route exact path='/authorization/login' component={PrincipalAuthorizationPage} />
    <Route exact path='/authorization//lock/profile' />
</Switch>
