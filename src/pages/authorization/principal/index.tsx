import { LoginFormState } from '../../../components/forms/login/types'
import { ReactComponent as CloudSvg } from '../../../images/cloud.svg'
import { LoginForm } from '../../../components/forms/login'
import { SimpleLayout } from '../../../layouts/simple'
import React, { useState } from 'react'

import './style.sass'

export const PrincipalAuthorizationPage = () => {

    const [principalAuthorizationType, setPrincipalAuthorizationType] = useState<string>('register')

    const onSubmitForm = (data: LoginFormState) => {
        console.log(data)
    }

    return (
        <SimpleLayout>
            <section className={`principal-authorization ${principalAuthorizationType}`}>
                <section className='principal-authorization-forms'>
                    <section className='principal-authorization-forms-descriptions'>
                        <CloudSvg className='cloud' />
                        <h1>PQRCloud</h1>
                    </section>
                    <LoginForm onSubmitForm={onSubmitForm} />
                </section>
            </section>
        </SimpleLayout>
    )

}