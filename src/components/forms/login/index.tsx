import { LoginFormProperties, LoginFormState } from './types'
import React, { useState } from 'react'

import './style.sass'

export const LoginForm = ({ onSubmitForm }: LoginFormProperties) => {

    const [formState, setFormState] = useState<LoginFormState>({ username: '', password: '', rememberMe: false })

    const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault()
		onSubmitForm(formState as LoginFormState)
        
    }

    const onChangeInput = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
		switch(target.type) {
			case 'checkbox':
				let { rememberMe } = formState
				setFormState({ ...formState, rememberMe: !rememberMe })
				break
			default:
				setFormState({ ...formState, [target.name]: target.value })
				break
		}
    } 

    return (
        <section className='login-form'>

            <form className='form-component' onSubmit={onSubmit} autoComplete='off'>

                <section className='input-text'>
                    <label htmlFor='username-input-text'>
                        <input onChange={onChangeInput} id='username-input-text' name='username' type='text' placeholder='Ingresar Usuario'/>
                    </label>
                </section>

                <section className='input-text'>
                    <label htmlFor='password-input-text'>
                        <input onChange={onChangeInput} id='password-input-text' name='password' type='password' placeholder='Ingresar Contraseña'/>
                    </label>
                </section>

                <section className='input-checkbox'>
                    <label htmlFor='remember-me-input-checkbox'>
                        <input onChange={onChangeInput} checked={formState.rememberMe} id='remember-me-input-checkbox' name='rememberMe' type='checkbox'/>
                        <span>¿Recuerdame?</span>
                    </label>
                </section>

                <section className='submit-form'>
                    <button type='submit'>
                        Ingresar
                    </button>
                </section>

            </form>

        </section>
    )

}