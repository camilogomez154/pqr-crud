export type LoginFormProperties = {
    onSubmitForm: (data: LoginFormState) => void
}

export type LoginFormState = {
    rememberMe: boolean
    username: string
    password: string
}