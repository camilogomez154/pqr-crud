export type GeneralTypeKey<T extends any> = {
    [x: string]: T
}