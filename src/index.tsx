import * as serviceWorker from './settings/serviceWorker'
import { RouterManager } from './pages'
import ReactDOM from 'react-dom'
import React from 'react'

import './styles/main.sass'

ReactDOM.render(<RouterManager />,document.getElementById('root'))
serviceWorker.unregister()