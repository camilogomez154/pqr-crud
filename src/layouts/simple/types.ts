export type SimpleLayoutProperties = {
    children?: JSX.Element
}