import { SimpleLayoutProperties } from "./types";
import * as React from "react";

import './style.sass'

export const SimpleLayout = ({ children }: SimpleLayoutProperties) => <div className='simple-layout'>{children}</div>
